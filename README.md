FAMILY BUDGET APP
_____________________________________________________
Problem Statement
_____________________________________________________
App should help track family (could be one or few persons) budget flow.
1. App keeps record of expenses and income.
2. Expense and income records contain info on a category, date and name of person.
3. App provides a way to add record categories on top of built-in ones, names of people
who will be using this app, review past operations, check monthly balance*. 
(Some functionality might not be available yet).
4. There is expense review by category possibility.

_______________________________________________________
Deployment procedure
________________________________________________________
App is built on Java 8, also requires a MySql database. Script for creating a schema is 
provided in resource repository.

Git clone: https://gitlab.com/u.raimondas/practical-project-bydgetapp.git
_________________________________________________________

Launch Procedure
_________________________________________________________
Run DataBase script from folder:
dbscripts/FamilyBudgetScript.sql

To run the app, run:
AppMain class



