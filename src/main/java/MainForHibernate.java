
import model.Expense;
import model.Income;
import model.IncomeCategory;
import model.Person;
import repository.*;
import util.HibernateUtil;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainForHibernate {

    public static void main(String[] args) {
        PersonRepository personRepository = new PersonRepository();
        IncomeCategoryRepository incomeCategoryRepository = new IncomeCategoryRepository();

        IncomeRepository incomeRepository = new IncomeRepository();
        ExpenseCategoryRepository expenseCR = new ExpenseCategoryRepository();
        Person person = personRepository.findByName("Antanas");

        ExpenseRepository expenseRepository = new ExpenseRepository();
        double amount = expenseRepository.getExpenseSumByCategory("Food", expenseCR);
        System.out.println(amount);
        System.out.println(expenseCR.findByName("Food"));
        List<Expense> listOfExpenses = expenseRepository.findExpenseListByPersonByByMonth(personRepository,"Antanas");
        for(Expense e:listOfExpenses){
            System.out.println(e);
        }
        System.out.println(expenseRepository.getExpenseSumFromExpenseList(listOfExpenses));


        //IncomeCategory incomeCategory = incomeCategoryRepository.findByName("FOOD");


//            Person maryte = new Person();
//            maryte.setName("Maryte");
//        personRepository.updatePerson(2,maryte);
       // ExpenseCategory expenseCategory2 = new ExpenseCategory();
       // expenseCategory2.setexpCategoryName("Family");
       // expenseCR.save(expenseCategory2);


        System.out.println(person.getPersonId());
        //System.out.println(incomeCategory.getCategoryId());

//
       //for(Person p: personRepository.findAll()) System.out.println(person);

        HibernateUtil.shutdown();

    }


}
