package Controller;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.*;
import repository.*;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class Controller {

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @FXML
    Button expenseInputButton;
    @FXML
    private TextField incomeAmountField;
    @FXML
    private TextField expenseAmountField;
    @FXML
    Tab tab;
    @FXML
    TextField currentDateField;
    @FXML
    private ComboBox<ExpenseCategory> expenseCategoriesBox;
    @FXML
    private ComboBox<String> filterExpensesByName;
    @FXML
    private ComboBox<IncomeCategory> incomeCategoriesBox;
    @FXML
    private ComboBox<Person> nameComboBox;
    @FXML
    private TableView<ExpenseData> expenseDataTable;
    @FXML
    private TableColumn<String,ExpenseData> expenseCategoryColumn;
    @FXML
    private TableColumn<Double, ExpenseData> expenseAmountColumn;
    @FXML
    private TableColumn<LocalDate, ExpenseData> expenseDate;
    @FXML TableColumn<String, ExpenseData> personName;




    private ExpenseData expenseData = new ExpenseData();
    private List<ExpenseData> expenseDataList = new ArrayList<>();
    ObservableList<ExpenseData> expenseDataObservableList = FXCollections.observableArrayList(expenseDataList);
    private final ExpenseCategoryRepository expenseCategoryRepository = new ExpenseCategoryRepository();
    private final IncomeCategoryRepository incomeCategoryRepository = new IncomeCategoryRepository();
    private final IncomeRepository incomeRepository = new IncomeRepository();
    private final ExpenseRepository expenseRepository = new ExpenseRepository();
    private final PersonRepository personRepository = new PersonRepository();
    private final NumberFormat nf = NumberFormat.getInstance();
    private ObservableList<String> filterBoxData = FXCollections.observableArrayList();




    public void initialize(){
        expenseCategoriesBox.getItems().addAll(expenseCategoryRepository.findAll());
        nameComboBox.getItems().addAll(personRepository.findAll());
        List<Person> temp =  personRepository.findAll();
        List<String> temp2 = new ArrayList<>();
        for(Person p:temp){
            temp2.add(p.getName());
        }
        filterBoxData = FXCollections.observableArrayList(temp2);
        filterExpensesByName.getItems().addAll(filterBoxData);
        currentDateField.setText(LocalDate.now().toString());
        List<Expense> expenseList = expenseRepository.findAll();
        expenseCategoryColumn.setCellValueFactory(new PropertyValueFactory<>("categoryNames"));
        expenseAmountColumn.setCellValueFactory(new PropertyValueFactory<>("amounts"));
        expenseDate.setCellValueFactory(new PropertyValueFactory<>("expenseDate"));
        personName.setCellValueFactory(new PropertyValueFactory<>("personNames"));



        Collections.reverse(expenseList);
        for(Expense e:expenseList){
            ExpenseData exdt = new ExpenseData();
            exdt.setAmounts(e.getExpenseAmount());
            exdt.setCategoryNames(e.getExpenseCategory().getExpCategoryName());
            exdt.setExpenseDate(e.getExpenseDate());
            exdt.setPersonNames(e.getPerson().getName());
            expenseDataObservableList.add(exdt);
            expenseDataTable.getItems().add(new ExpenseData(e.getExpenseCategory().getExpCategoryName(),
                    e.getPerson().getName(), e.getExpenseDate(), e.getExpenseAmount()));
        }

    }



    @FXML
    public void handleIncomeInput(){
        nf.setMaximumFractionDigits(2);
        Income income = new Income();
        income.setPerson(personRepository.findByName(tab.getText()));
        income.setIncomeDate(LocalDate.now());
        income.setIncomeCategory(incomeCategoriesBox.getSelectionModel().getSelectedItem());
        try{
            double amount = nf.parse(incomeAmountField.getCharacters().toString()).doubleValue();
            nf.setMaximumFractionDigits(2);
            String output = nf.format(amount);
            income.setIncomeAmount(nf.parse(output).doubleValue());

        }catch(ParseException e){
            System.out.println("Wrong input");
        }
        incomeRepository.save(income);
    }



    @FXML
    public void handleExpenseInput(){
        nf.setMaximumFractionDigits(2);
        Expense expense = new Expense();
        if(!(nameComboBox.getSelectionModel().getSelectedItem() == null | expenseCategoriesBox.getSelectionModel().getSelectedItem() == null)) {
            expense.setPerson(nameComboBox.getSelectionModel().getSelectedItem());
            expense.setExpenseDate(LocalDate.now());
            expense.setExpenseCategory(expenseCategoriesBox.getSelectionModel().getSelectedItem());
            try {
                double amount = nf.parse(expenseAmountField.getCharacters().toString()).doubleValue();
                nf.setMaximumFractionDigits(2);
                String output = nf.format(amount);
                expense.setExpenseAmount(nf.parse(output).doubleValue());
            } catch (ParseException e) {
                System.out.println("Wrong input");
            }
            expenseRepository.save(expense);
            //Takes reversed collection and reverses it back, adds a new element, then reverses again and shows in the tabble
            FXCollections.reverse(expenseDataObservableList);
            expenseDataObservableList.add(new ExpenseData(expense.getExpenseCategory().getExpCategoryName(),
                    expense.getPerson().getName(), expense.getExpenseDate(),
                    expense.getExpenseAmount()));
            FXCollections.reverse(expenseDataObservableList);

            expenseAmountField.clear();
            System.out.println("button clicked");
        }
    }
    @FXML
    public void handleExpenseFiltering() {
        String selectedPerson = filterExpensesByName.getSelectionModel().getSelectedItem();
        if (selectedPerson.equals("All")){
            expenseDataTable.setItems(expenseDataObservableList);
         } else{
            List<ExpenseData> filteredList = expenseDataObservableList.
                    stream().
                    filter(ed-> ed.getPersonNames().equals(selectedPerson)).collect(Collectors.toList());
            ObservableList<ExpenseData> observableFilteredList = FXCollections.observableArrayList(filteredList);
            expenseDataTable.setItems(observableFilteredList);
        }
    }




}
