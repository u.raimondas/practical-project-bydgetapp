package repository;


import model.ExpenseCategory;
import model.IncomeCategory;
import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;

public class IncomeCategoryRepository {

    public List<IncomeCategory> findAll(){
        List<IncomeCategory> result = new ArrayList<>();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery("from IncomeCategory", IncomeCategory.class).list();
            //System.out.println("New person inserted");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert  session != null;
            session.close();

        }
        return result;
    }

    public IncomeCategory findById(int id){
        IncomeCategory result = new IncomeCategory();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(IncomeCategory.class, id);
            //System.out.println("find by id");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }

    public IncomeCategory findByName(String categoryName){
        IncomeCategory result = new IncomeCategory();
        IncomeCategory p = new IncomeCategory();
        p.setIncomeCategoryName(categoryName);
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from IncomeCategory ic where ic.incomeCategoryName = :incomeCategoryName";
            result = (IncomeCategory) session.createQuery(hql).setProperties(p).getSingleResult();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }



    public void save(IncomeCategory incomeCategory){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(incomeCategory);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void delete (int id){
        Session session= null;
        ExpenseCategory result = new ExpenseCategory();
        try{

            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            result = session.find(ExpenseCategory.class, id);
            session.delete(result);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

    public void updateIncomeCategory (int id, IncomeCategory incomeCategory) {
        Session session = null;
        Person result;
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(Person.class, id);
            Transaction transaction = session.beginTransaction();
            result.setName(incomeCategory.getIncomeCategoryName());
            //session.save(result);
            session.update(result);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

}


