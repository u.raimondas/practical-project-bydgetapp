package repository;


import model.Expense;
import model.ExpenseCategory;
import model.IncomeCategory;
import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExpenseCategoryRepository {


    //find all expense categories (expense categories object list)
    public List<ExpenseCategory> findAll(){
        List<ExpenseCategory> result = new ArrayList<>();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery("from ExpenseCategory", ExpenseCategory.class).list();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert  session != null;
            session.close();

        }
        return result;
    }

    //find all expense category names (string)
    public List<String> findAllCategoryNames(ExpenseCategoryRepository repository){
        List<ExpenseCategory> expenseCategoryList = repository.findAll();
        List<String> categoryNames = new ArrayList<>();
        for (ExpenseCategory expenseCategory : expenseCategoryList)
            categoryNames.add(expenseCategory.getExpCategoryName());
        return categoryNames;
    }

//    public List<String> findAllCategoryNames(List<ExpenseCategory> expenseCategoryList){
//        List<String> categoryNames = new ArrayList<>();
//        expenseCategoryList.stream().forEach(expenseCategory -> categoryNames.add(expenseCategory.getExpCategoryName()));
//        return categoryNames;
//    }



    //Find expense category by id
    public ExpenseCategory findById(int id){
        ExpenseCategory result = new ExpenseCategory();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(ExpenseCategory.class, id);
            //System.out.println("find by id");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }

    //find expense category by name (string)

    public ExpenseCategory findByName(String categoryName){
        ExpenseCategory result = new ExpenseCategory();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from ExpenseCategory ec where ec.expenseCategoryName = :expenseCategoryName";
            Query query = session.createQuery(hql);
            query.setParameter("expenseCategoryName",categoryName);
            result = (ExpenseCategory) query.uniqueResult();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }


    //save expense category to database
    public void save(ExpenseCategory expenseCategory){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(expenseCategory);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //delete expense category from database (need to test)
    public void delete (int id){
        Session session= null;
        ExpenseCategory result;
        try{

            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            result = session.find(ExpenseCategory.class, id);
            session.delete(result);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

    //update expense category in database
    public void updateExpenseCategory (int id, ExpenseCategory expenseCategory) {
        Session session = null;
        Person result;
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(Person.class, id);
            Transaction transaction = session.beginTransaction();
            result.setName(expenseCategory.getExpCategoryName());
            session.update(result);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

}

