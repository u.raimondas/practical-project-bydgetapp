package repository;

import model.Income;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class IncomeRepository {


    public void save(Income income){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(income);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
