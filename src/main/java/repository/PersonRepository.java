package repository;

import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {

    public List<Person> findAll(){
        List<Person> result = new ArrayList<>();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery("from Person", Person.class).list();
            //System.out.println("New person inserted");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert  session != null;
                session.close();

        }
        return result;
    }

    public Person findById(int id){
        Person result = new Person();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(Person.class, id);
            //System.out.println("find by id");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }

    public Person findByName(String personName){
        Person result = new Person();
        Person p = new Person();
        p.setName(personName);
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from Person p where p.name = :name";
            result = (Person) session.createQuery(hql).setProperties(p).getSingleResult();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }



    public void save(Person person){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(person);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void delete (int id){
        Session session= null;
        Person result = new Person();
        try{

            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            result = session.find(Person.class, id);
            session.delete(result);
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

    public void updatePerson (int id, Person person) {
        Session session = null;
        Person result;
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            result = session.find(Person.class, id);
            Transaction transaction = session.beginTransaction();
            result.setName(person.getName());
            //session.save(result);
            session.update(result);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
    }

}
