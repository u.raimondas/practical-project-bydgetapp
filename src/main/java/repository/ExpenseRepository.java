package repository;

import model.Expense;
import model.ExpenseCategory;
import model.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ExpenseRepository {


    public void save(Expense expense){
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(expense);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // dar ne galutinis metodas. kol kas grąžina tik išlaidų sąrašą. reikia padaryti, kad grąžintų išlaidų sumą pagal
    // kategorijas. gal vertėtų daryti map<expensecategory,amount>. tik kažin kaip map'as būtų naudoti toliau
    //pildant stulpelius lentelėje.
    public List<Expense> findExpenseListByPersonByByMonth(PersonRepository repository, String name){
        Person person = repository.findByName(name);
        List<Expense> expenseList = person.getExpenses();
        List<Expense> list = new ArrayList<>();
        for (Expense expense : expenseList)
            if (expense.getExpenseDate().getMonth().equals(LocalDate.now().getMonth()) &
                    expense.getExpenseDate().getYear()==(LocalDate.now().getYear())) list.add(expense);
        return list;
    }

    public List<Expense> findAll(){
        List<Expense> result = new ArrayList<>();
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            result = session.createQuery("from Expense", Expense.class).list();
            //System.out.println("New person inserted");

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert  session != null;
            session.close();

        }
        return result;
    }

    public double getExpenseSumByCategory(String expenseCategory, ExpenseCategoryRepository ecr){
        ExpenseCategory ec= ecr.findByName(expenseCategory);
        int id = ec.getCategoryId();
        Session session = null;
        double result = 0;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            //select sum(expenses_amount) from expenses
            //where category_id=6;
            String hql = "select sum(expenseAmount) from Expense where expenseCategory = :ec";
            Query query = session.createQuery(hql);
            query.setParameter("ec",ec);
            result = (double) query.uniqueResult();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            assert session != null;
            session.close();
        }
        return result;
    }

    //find expense amount from expense list

    public double getExpenseSumFromExpenseList(List<Expense> expenseList){
        double expenseSum = 0;
        for(Expense e:expenseList){
            expenseSum+=e.getExpenseAmount();
        }
        return expenseSum;
    }
}
