import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static javafx.fxml.FXMLLoader.load;

public class AppMain extends Application {

    private Stage stage;

    public static void main(String[] args) {

        AppMain.launch();
    }

    @Override
    public void init() throws Exception {
        System.out.println("Initializing app");
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
//        PersonRepository personRepository = new PersonRepository();
//        for(Person person: personRepository.findAll()) System.out.println(person);
        Parent root = FXMLLoader.load(getClass().getResource("/AppView.fxml"));
//        GridPane root = new GridPane();
//        root.setAlignment(Pos.CENTER);
//        root.setVgap(5);
//        root.setHgap(10);
//        Label greeting = new Label("Welcome to Family Budget planner");
//        root.getChildren().addAll(greeting);
//        greeting.setTextFill(Color.GREEN);
//        greeting.setFont(Font.font("Times New Roman", FontWeight.BOLD, 20));

        primaryStage.setTitle("Welcome to Family budget");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

//        System.out.println("Starting app");
//        // Creating the container object
//        VBox vBox = new VBox(10);
//        // Creating a label control. Constructor argument represents displayed text.
//        Label label = new Label("Family Budget");
//        label.setFont(Font.font(30));
//        // Setting label to be a child of the vBox container
//        vBox.getChildren().add(label);
//        Button button = new Button("Father");
//        vBox.getChildren().add(button);
//        // Creating a scene. The vBox is passed as it's root.
//        Scene scene = new Scene(vBox, 300,300);
//        //scene.getFill().isOpaque();
//        // Setting the main window's scene.
//        primaryStage.setScene(scene);
//        // Showing the window.
//        primaryStage.show();
    }
    @Override
    public void stop() throws Exception {
        // this will be executed even if Exception is thrown at runtime
       // HibernateUtil.shutdown();
        System.out.println("Stopping app");
    }
}
