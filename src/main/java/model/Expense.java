package model;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "expenses")
public class Expense {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "expenses_id")
        private int expenseId;

        @Column (name = "expenses_amount")
        private double expenseAmount;

        @Column (name = "expenses_date")
        private LocalDate expenseDate;

        @ManyToOne (fetch = FetchType.EAGER)
        @JoinColumn (name="person_id")
        private Person person;

        @ManyToOne (fetch = FetchType.EAGER)
        @JoinColumn (name = "category_id")
        private ExpenseCategory expenseCategory;



    public int getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(int expenseId) {
        this.expenseId = expenseId;
    }

    public double getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(double expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public LocalDate getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(LocalDate expenseDate) {
        this.expenseDate = expenseDate;
    }

    public ExpenseCategory getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(ExpenseCategory expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return  expenseAmount + " " + expenseDate + " " + person;
    }
}
