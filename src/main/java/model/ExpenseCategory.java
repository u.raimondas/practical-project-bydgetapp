package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "expenses_category")
public class ExpenseCategory {



        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column (name = "category_id")
        private int categoryId;

        @Column (name = "expenses_category_name")
        private String expenseCategoryName;

        @OneToMany (mappedBy = "expenseCategory")
        private List<Expense> expenseList = new ArrayList<>();

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int id) {
            this.categoryId = id;        }

        public String getExpCategoryName() {
            return expenseCategoryName;
        }

        public void setExpenseCategoryName(String name) {
            this.expenseCategoryName = name;
        }

    public List<Expense> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<Expense> expenseList) {
        this.expenseList = expenseList;
    }

    @Override
        public String toString() {
            return expenseCategoryName;
        }
}


