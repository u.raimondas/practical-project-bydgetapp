package model;

import repository.ExpenseCategoryRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ExpenseData {

    private String categoryNames;
    private String personNames;
    private LocalDate expenseDate;
    private Double amounts;

    public ExpenseData(String categoryNames, String personNames, LocalDate expenseDate, Double amounts) {
        this.categoryNames = categoryNames;
        this.personNames = personNames;
        this.expenseDate = expenseDate;
        this.amounts = amounts;
    }

    public ExpenseData() {
    }

    public String getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(String categoryNames) {
        this.categoryNames = categoryNames;
    }

    public String getPersonNames() {
        return personNames;
    }

    public void setPersonNames(String personNames) {
        this.personNames = personNames;
    }

    public LocalDate getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(LocalDate expenseDate) {
        this.expenseDate = expenseDate;
    }

    public Double getAmounts() {
        return amounts;
    }

    public void setAmounts(Double amounts) {
        this.amounts = amounts;
    }
}
