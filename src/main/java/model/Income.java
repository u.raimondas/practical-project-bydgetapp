package model;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "income")
public class Income {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "income_id")
    private int incomeId;

    @Column (name = "income_amount")
    private double incomeAmount;

    @Column (name = "income_date")
    private LocalDate incomeDate;


    @Override
    public String toString() {
        return incomeAmount + " " + incomeDate + " " + person;
    }


    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn (name="person_id")
    private Person person;

    @ManyToOne
    @JoinColumn (name = "category_id")
    private IncomeCategory incomeCategory;

    public IncomeCategory getIncomeCategory() {
        return incomeCategory;
    }

    public void setIncomeCategory(IncomeCategory incomeCategory) {
        this.incomeCategory = incomeCategory;
    }

    public int getIncomeId() {
        return incomeId;
    }

    public void setIncomeId(int incomeId) {
        this.incomeId = incomeId;
    }

    public double getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(double incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public LocalDate getIncomeDate() {
        return incomeDate;
    }

    public void setIncomeDate(LocalDate incomeDate) {
        this.incomeDate = incomeDate;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
