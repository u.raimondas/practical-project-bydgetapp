package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "income_category")
public class IncomeCategory {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "category_id")
    private int categoryId;

    @Column (name = "income_category_name")
    private String incomeCategoryName;

    @OneToMany (mappedBy="incomeCategory")
    List<Income> incomeList = new ArrayList<>();

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int id) {
        this.categoryId = id;
    }

    public String getIncomeCategoryName() {
        return incomeCategoryName;
    }

    public void setIncomeCategoryName(String name) {
        this.incomeCategoryName = name;
    }

    public List<Income> getIncomeList() { return incomeList;    }

    public void setIncomeList(List<Income> incomeList) { this.incomeList = incomeList; }

    @Override
    public String toString() {
        return incomeCategoryName;
    }
}



