create schema family_budget;
use family_budget;


CREATE TABLE person (
    person_id INT NOT NULL AUTO_INCREMENT,
    person_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (person_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE expenses (
    expenses_id INT NOT NULL AUTO_INCREMENT,
    expenses_amount DOUBLE NOT NULL,
    expenses_date DATE,
    category_id int,
    person_id int,
    PRIMARY KEY (expenses_id),
    constraint fk_expenses_person FOREIGN KEY (person_id) references person (person_id) on update no action on delete no action,
	constraint fk_expenses_expenses_category FOREIGN KEY (category_id) references expenses_category (category_id) on update no action on delete no action
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE expenses_category(
    category_id INT NOT NULL AUTO_INCREMENT,
    expenses_category_name varchar(50) NOT NULL,
    PRIMARY KEY (category_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

alter table expenses
add constraint fk_expenses_person 
	FOREIGN KEY (person_id)
    references person (person_id);

CREATE TABLE income_category (
    category_id INT NOT NULL AUTO_INCREMENT,
    income_category_name varchar (50) NOT NULL,
    PRIMARY KEY (category_id)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;


CREATE TABLE income (
    income_id INT NOT NULL AUTO_INCREMENT,
    income_amount DOUBLE NOT NULL,
    income_date DATE NOT NULL,
    category_id int,
    person_id int,
    PRIMARY KEY (income_id),
    constraint fk_income_income_category 
	FOREIGN KEY (category_id)
    references income_category(category_id) on update no action on delete no action,
    constraint fk_income_person  FOREIGN KEY (person_id)  references person (person_id) on update no action on delete no action
)  ENGINE=INNODB DEFAULT CHARSET=UTF8MB4;

alter table income
add constraint fk_income_income_category 
	FOREIGN KEY (category_id)
    references income_category(category_id) on update no action on delete no action;
    
alter table income
add constraint fk_income_person 
	FOREIGN KEY (person_id)
    references person (person_id);






